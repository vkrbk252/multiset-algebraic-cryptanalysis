#include "FunctionsForAttacks.h"

void InvSubBytes(uint8_t *block)
{
	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		block[i] = InvSbox[block[i]];
	}
}

void AddRoundKey(uint8_t *block, uint8_t *RoundKey)
{
	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		block[i] ^= RoundKey[i];
	}
}

void LinearTransform(uint8_t *block)
{
	uint8_t *result = new uint8_t[BLOCK_SIZE]{ 0 };

	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		for (int j = 0; j < BLOCK_SIZE; j++)
		{
			result[i] ^= Mul_Matrix[block[j]][L[j][i]];
		}
	}

	memcpy(block, result, BLOCK_SIZE);

	delete[]result;
}
