#include "LSX_Cipher.h"

LSX_Cipher::LSX_Cipher()
{
	const size_t block_size = 8;
	const size_t number_of_rounds = 6;
	uint8_t key[block_size * 2] = { 0x08, 0x08, 0x09, 0x09, 0x0a, 0x0a, 0x0b, 0x0b, 
	                                0x0c, 0x0c, 0x0d, 0x0d, 0x0e, 0x0e, 0x0f, 0x0f };
	this->SetRoundKeys(block_size, number_of_rounds, key);
}

LSX_Cipher::LSX_Cipher(size_t block_size, size_t number_of_rounds, uint8_t *key)
{
	this->SetRoundKeys(block_size, number_of_rounds, key);
}

LSX_Cipher::~LSX_Cipher()
{
	// free memory for constants
	for (size_t i = 0; i < ((NUMBER_OF_ROUNDS / 2) * 8); i++)
	{
		delete[]Const[i];
	}
	delete[]Const;

	// free memory for RoundKeys
	for (size_t i = 0; i < ((NUMBER_OF_ROUNDS / 2) * 2) + 2; i++)
	{
		delete[]RoundKey[i];
	}
	delete[]RoundKey;
}

void LSX_Cipher::SetRoundKeys(size_t block_size, size_t number_of_rounds, uint8_t *key)
{
	BLOCK_SIZE = block_size;
	NUMBER_OF_ROUNDS = number_of_rounds;

	// allocate memory for constants
	Const = new uint8_t*[(NUMBER_OF_ROUNDS / 2) * 8];
	for (size_t i = 0; i < ((NUMBER_OF_ROUNDS / 2) * 8); i++)
	{
		Const[i] = new uint8_t[BLOCK_SIZE];
	}

	// allocate memory for RoundKeys
	RoundKey = new uint8_t*[((NUMBER_OF_ROUNDS / 2) * 2) + 2];
	for (size_t i = 0; i < ((NUMBER_OF_ROUNDS / 2) * 2) + 2; i++)
	{
		RoundKey[i] = new uint8_t[BLOCK_SIZE];
	}

	KeyExpansion(key);
}

void LSX_Cipher::VariableToBlock(size_t x, uint8_t *tmp_block)
{
	uint8_t *block = new uint8_t[BLOCK_SIZE / 2];
	int type_size = sizeof(x);

	for (int i = ((BLOCK_SIZE / 2) - 1), j = 0; i >= 0; i--, j += 8)
	{
		if (j > ((type_size * 8) - 8))
			j = (type_size * 8);

		block[i] = (uint8_t)(x >> j);
	}

	for (size_t i = 0; i < BLOCK_SIZE; i += 2)
	{
		tmp_block[i] = (block[i / 2] >> 4) & 0x0f;
		tmp_block[i + 1] = (block[i / 2]) & 0x0f;
	}

	delete[]block;
}

void LSX_Cipher::xor_blocks(uint8_t *block1, uint8_t *block2)
{
	for (size_t i = 0; i < BLOCK_SIZE; i++)
	{
		block1[i] ^= block2[i];
	}
}

void LSX_Cipher::KeyExpansion(uint8_t *key)
{
	uint8_t *tmp_block = new uint8_t[BLOCK_SIZE];

	// Calculate constants for round keys
	for (size_t i = 0; i < ((NUMBER_OF_ROUNDS / 2) * 8); i++)
	{
		VariableToBlock(i + 1, tmp_block);

		LinearTransform(tmp_block);

		memcpy(Const[i], tmp_block, BLOCK_SIZE);
	}

	delete[]tmp_block;

	// Calculate round keys
	memcpy(RoundKey[0], key, BLOCK_SIZE);
	memcpy(RoundKey[1], key + BLOCK_SIZE, BLOCK_SIZE);

	uint8_t *tmp_block_1 = new uint8_t[BLOCK_SIZE];
	uint8_t *tmp_block_2 = new uint8_t[BLOCK_SIZE];

	for (size_t i = 0, j = 0; i < ((NUMBER_OF_ROUNDS / 2) * 2); i += 2, j += 8)
	{
		memcpy(tmp_block_1, RoundKey[i], BLOCK_SIZE);
		memcpy(tmp_block_2, RoundKey[i + 1], BLOCK_SIZE);

		for (size_t NC = 0; NC < 8; NC++)
		{
			F((j + NC), tmp_block_1, tmp_block_2);
		}

		memcpy(RoundKey[i + 2], tmp_block_1, BLOCK_SIZE);
		memcpy(RoundKey[i + 3], tmp_block_2, BLOCK_SIZE);
	}

	delete[]tmp_block_1;
	delete[]tmp_block_2;
}

void LSX_Cipher::F(size_t n, uint8_t *RoundKey1, uint8_t *RoundKey2)
{
	uint8_t *tmp_block = new uint8_t[BLOCK_SIZE];

	memcpy(tmp_block, RoundKey1, BLOCK_SIZE);
	
	xor_blocks(RoundKey1, Const[n]);
	SubBytes(RoundKey1);
	LinearTransform(RoundKey1);

	xor_blocks(RoundKey1, RoundKey2);

	memcpy(RoundKey2, tmp_block, BLOCK_SIZE);

	delete[]tmp_block;
}

void LSX_Cipher::AddRoundKey(uint8_t *block, uint8_t *RoundKey)
{
	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		block[i] ^= RoundKey[i];
	}
}

void LSX_Cipher::SubBytes(uint8_t *block)
{
	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		block[i] = Sbox[block[i]];
	}
}

void LSX_Cipher::LinearTransform(uint8_t *block)
{
	uint8_t *result = new uint8_t[BLOCK_SIZE]{ 0 };

	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		for (int j = 0; j < BLOCK_SIZE; j++)
		{
			result[i] ^= Mul_Matrix[block[j]][L[j][i]];
		}
	}

	memcpy(block, result, BLOCK_SIZE);

	delete[]result;
}

void LSX_Cipher::EncryptBlock(uint8_t *input, uint8_t *output)
{
	uint8_t *block = new uint8_t[BLOCK_SIZE];

	memcpy(block, input, BLOCK_SIZE);

	for (int round = 0; round < NUMBER_OF_ROUNDS; round++)
	{
		AddRoundKey(block, RoundKey[round]);
		SubBytes(block);
		LinearTransform(block);
	}
	AddRoundKey(block, RoundKey[NUMBER_OF_ROUNDS]);

	memcpy(output, block, BLOCK_SIZE);

	delete[]block;
}

void LSX_Cipher::Encrypt_Block_Equivalent_Presentation_Of_The_Last_Round(uint8_t *input, uint8_t *output)
{
	uint8_t *block = new uint8_t[BLOCK_SIZE];

	memcpy(block, input, BLOCK_SIZE);

	for (int round = 0; round < NUMBER_OF_ROUNDS; round++)
	{
		AddRoundKey(block, RoundKey[round]);
		SubBytes(block);
		LinearTransform(block);
	}
	AddRoundKey(block, RoundKey[NUMBER_OF_ROUNDS]);

	InvLinearTransform(block); // L^(-1)(B)

	memcpy(output, block, BLOCK_SIZE);

	delete[]block;
}

void LSX_Cipher::Encrypt_Block_Equivalent_Presentation_Two_Last_Rounds(uint8_t *input, uint8_t *output)
{
	uint8_t *block = new uint8_t[BLOCK_SIZE];

	memcpy(block, input, BLOCK_SIZE);

	for (int round = 0; round < (NUMBER_OF_ROUNDS - 2); round++)
	{
		AddRoundKey(block, RoundKey[round]);
		SubBytes(block);
		LinearTransform(block);
	}
	AddRoundKey(block, RoundKey[(NUMBER_OF_ROUNDS - 2)]);

	uint8_t *InvPenultimateK = new uint8_t[BLOCK_SIZE];
	uint8_t *InvLastK = new uint8_t[BLOCK_SIZE];

	memcpy(InvPenultimateK, RoundKey[NUMBER_OF_ROUNDS - 1], BLOCK_SIZE);
	memcpy(InvLastK, RoundKey[NUMBER_OF_ROUNDS], BLOCK_SIZE);

	InvLinearTransform(InvPenultimateK);
	InvLinearTransform(InvLastK);

	SubBytes(block);
	AddRoundKey(block, InvPenultimateK);
	LinearTransform(block);
	SubBytes(block);
	AddRoundKey(block, InvLastK);

	memcpy(output, block, BLOCK_SIZE);

	delete[]InvPenultimateK;
	delete[]InvLastK;
	delete[]block;
}

/****************************************************DECRYPT******************************************************/

void LSX_Cipher::DecryptBlock(uint8_t *input, uint8_t *output)
{
	uint8_t *block = new uint8_t[BLOCK_SIZE];

	memcpy(block, input, BLOCK_SIZE);

	AddRoundKey(block, RoundKey[NUMBER_OF_ROUNDS]);
	for (int round = (NUMBER_OF_ROUNDS - 1); round >= 0; round--)
	{
		InvLinearTransform(block);
		InvSubBytes(block);
		AddRoundKey(block, RoundKey[round]);
	}

	memcpy(output, block, BLOCK_SIZE);

	delete[]block;
}

void LSX_Cipher::InvSubBytes(uint8_t *block)
{
	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		block[i] = InvSbox[block[i]];
	}
}

void LSX_Cipher::InvLinearTransform(uint8_t *block)
{
	uint8_t *result = new uint8_t[BLOCK_SIZE]{ 0 };

	for (int i = 0; i < BLOCK_SIZE; i++)
	{
		for (int j = 0; j < BLOCK_SIZE; j++)
		{
			result[i] ^= Mul_Matrix[block[j]][InvL[j][i]];
		}
	}

	memcpy(block, result, BLOCK_SIZE);

	delete[]result;
}

// Secondary functions
/*********************************************************/
void LSX_Cipher::Print_Round_Keys()
{
	printf("RoundKeys: \n");
	for (size_t i = 0; i < ((NUMBER_OF_ROUNDS / 2) * 2) + 2; i++)
	{
		for (size_t j = 0; j < BLOCK_SIZE; j++)
		{
			printf("%x ", RoundKey[i][j]);
		}
		printf("\n\n");
	}
}

void LSX_Cipher::Print_Constants_For_Round_Keys()
{
	printf("Constants: \n");
	for (size_t i = 0; i < ((NUMBER_OF_ROUNDS / 2) * 8); i++)
	{
		for (size_t j = 0; j < BLOCK_SIZE; j++)
		{
			printf("%x ", Const[i][j]);
		}
		printf("\n\n");
	}
}
/*********************************************************/
