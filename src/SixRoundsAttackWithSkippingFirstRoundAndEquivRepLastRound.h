#ifndef SIX_ROUNDS_ATTACK_H_
#define SIX_ROUNDS_ATTACK_H_

#include "LSX_Cipher.h"
#include"FunctionsForAttacks.h"

void Six_Rounds_Attack_With_Skipping_First_Round_And_Equiv_Rep_Last_Round()
{
	constexpr size_t NUMBER_OF_ROUNDS = 6;

	uint8_t plain_text[BLOCK_SIZE] = { 0 };
	uint8_t cipher_text[BLOCK_SIZE] = { 0 };
	uint8_t key[BLOCK_SIZE * 2] = { 0x08, 0x08, 0x09, 0x09, 0x0a, 0x0a, 0x0b, 0x0b, 
	                                0x0c, 0x0c, 0x0d, 0x0d, 0x0e, 0x0e, 0x0f, 0x0f };

	LSX_Cipher obj;
	obj.SetRoundKeys(BLOCK_SIZE, NUMBER_OF_ROUNDS, key);

	obj.Print_Round_Keys();

	size_t freq[BLOCK_SIZE][16] = { 0 };
	std::vector<std::vector<uint8_t>> b(BLOCK_SIZE);

	for (uint8_t i = 0; i < 4; i++)
	{
		plain_text[0] = i;
		plain_text[0] = InvSbox[plain_text[0]];
		plain_text[0] ^= 0x08;

		for (plain_text[1] = 0; plain_text[1] < 16; plain_text[1]++)
		{
			printf("%x%x \n\n", i, plain_text[1]);

			for (plain_text[2] = 0; plain_text[2] < 16; plain_text[2]++)
			{
				for (plain_text[3] = 0; plain_text[3] < 16; plain_text[3]++)
				{
					for (plain_text[4] = 0; plain_text[4] < 16; plain_text[4]++)
					{
						for (plain_text[5] = 0; plain_text[5] < 16; plain_text[5]++)
						{
							for (plain_text[6] = 0; plain_text[6] < 16; plain_text[6]++)
							{
								for (plain_text[7] = 0; plain_text[7] < 16; plain_text[7]++)
								{
									obj.Encrypt_Block_Equivalent_Presentation_Of_The_Last_Round(plain_text, cipher_text);

									for (size_t i = 0; i < BLOCK_SIZE; i++)
									{
										freq[i][cipher_text[i]]++;
									}
								}
							}
						}
					}
				}
			}
		}
	}

	printf("FREQ: \n\n");
	for (size_t i = 0; i < 16; i++)
	{
		printf("%x : ", i);
		for (size_t j = 0; j < BLOCK_SIZE; j++)
		{
			if (freq[j][i] == 0)
			{
				printf("     %u ", freq[j][i]);
			}
			else
			{
				printf("%u ", freq[j][i]);
			}
		}
		printf("\n\n");
	}

	for (size_t i = 0; i < BLOCK_SIZE; i++)
	{
		for (size_t j = 0; j < 16; j++)
		{
			if ((freq[i][j] % 2) != 0)
			{
				b[i].push_back(j);
			}
		}
	}

	printf("B: \n\n");
	for (size_t i = 0; i < b.size(); i++)
	{
		printf("size: %u \n", b[i].size());
		for (size_t j = 0; j < b[i].size(); j++)
		{
			printf("%x ", b[i][j]);
		}
		printf("\n\n");
	}

	std::vector<std::vector<uint8_t>> Possible_Inv_Round_Key(BLOCK_SIZE);

	for (uint8_t i = 0; i < BLOCK_SIZE; i++)
	{
		for (uint8_t Ki = 0; Ki < 16; Ki++)
		{
			uint8_t result[BLOCK_SIZE] = { 0 };

			for (int j = 0; j < b[i].size(); j++)
			{
				uint8_t block[BLOCK_SIZE] = { 0 };
				uint8_t RoundKey[BLOCK_SIZE] = { 0 };

				block[i] = b[i][j];
				RoundKey[i] = Ki;

				AddRoundKey(block, RoundKey);
				InvSubBytes(block);

				result[i] ^= block[i];
			}

			if (result[i] == 0)
			{
				Possible_Inv_Round_Key[i].push_back(Ki);
			}
		}
	}

	printf("POSSIBLE_INV_ROUND_KEY: \n\n");
	for (size_t i = 0; i < Possible_Inv_Round_Key.size(); i++)
	{
		printf("size: %u \n", Possible_Inv_Round_Key[i].size());
		for (size_t j = 0; j < Possible_Inv_Round_Key[i].size(); j++)
		{
			printf("%x ", Possible_Inv_Round_Key[i][j]);
		}
		printf("\n\n");
	}

	uint8_t block[BLOCK_SIZE] = { 0 };
	for (size_t i = 0; i < Possible_Inv_Round_Key[0].size(); i++)
	{
		block[0] = Possible_Inv_Round_Key[0][i];
		for (size_t j = 0; j < Possible_Inv_Round_Key[1].size(); j++)
		{
			block[1] = Possible_Inv_Round_Key[1][j];
			for (size_t k = 0; k < Possible_Inv_Round_Key[2].size(); k++)
			{
				block[2] = Possible_Inv_Round_Key[2][k];
				for (size_t l = 0; l < Possible_Inv_Round_Key[3].size(); l++)
				{
					block[3] = Possible_Inv_Round_Key[3][l];
					for (size_t m = 0; m < Possible_Inv_Round_Key[4].size(); m++)
					{
						block[4] = Possible_Inv_Round_Key[4][m];
						for (size_t n = 0; n < Possible_Inv_Round_Key[5].size(); n++)
						{
							block[5] = Possible_Inv_Round_Key[5][n];
							for (size_t p = 0; p < Possible_Inv_Round_Key[6].size(); p++)
							{
								block[6] = Possible_Inv_Round_Key[6][p];
								for (size_t r = 0; r < Possible_Inv_Round_Key[7].size(); r++)
								{
									block[7] = Possible_Inv_Round_Key[7][r];

									uint8_t block2[BLOCK_SIZE];
									memcpy(block2, block, BLOCK_SIZE);

									LinearTransform(block2);

									if ((block2[0] == 0x0a) && (block2[1] == 0x01) && 
									    (block2[2] == 0x06) && (block2[3] == 0x04) && 
									    (block2[4] == 0x08) && (block2[5] == 0x09) && 
									    (block2[6] == 0x01) && (block2[7] == 0x0e))
									{
										printf("RoundKey: ");
										Print_Array(block2);
									}

									//Print_Array(block2);
								}
							}
						}
					}
				}
			}
		}
	}
}

#endif /*SIX_ROUNDS_ATTACK_H_*/
