#include "LSXCipher.h"
#include "FunctionsForAttacks.h"
#include <iostream>
#include <vector>
#include <cmath>
#include "FiveRoundsAttackWithEquivLastRound.h"
#include "SixRoundsAttackWithSkippingFirstRoundAndEquivRepLastRound.h"
#include <omp.h>

constexpr size_t NUMBER_OF_ROUNDS = 6;

void LSX_Cipher_Test_1()
{
	uint8_t plain_text[BLOCK_SIZE] = { 0x01, 0x01, 0x02, 0x02, 0x03, 0x03, 0x04, 0x04 };
	uint8_t cipher_text[BLOCK_SIZE] = { 0 };
	uint8_t key[BLOCK_SIZE * 2] = { 0x08, 0x08, 0x09, 0x09, 0x0a, 0x0a, 0x0b, 0x0b, 
	                                0x0c, 0x0c, 0x0d, 0x0d, 0x0e, 0x0e, 0x0f, 0x0f };
	uint8_t decrypted_text[BLOCK_SIZE] = { 0 };

	LSX_Cipher obj;
	obj.SetRoundKeys(BLOCK_SIZE, NUMBER_OF_ROUNDS, key);

	obj.Print_Round_Keys();

	obj.EncryptBlock(plain_text, cipher_text);
	obj.DecryptBlock(cipher_text, decrypted_text);

	printf("PLAIN TEXT: \n");
	Print_Array(plain_text);

	printf("CIPHER TEXT: \n");
	Print_Array(cipher_text);

	printf("DECRYPTED TEXT: \n");
	Print_Array(decrypted_text);
}

void LSX_Cipher_Test_All_Texts()
{
	uint8_t plain_text[BLOCK_SIZE] = { 0 };
	uint8_t cipher_text[BLOCK_SIZE] = { 0 };
	uint8_t key[BLOCK_SIZE * 2] = { 0x08, 0x08, 0x09, 0x09, 0x0a, 0x0a, 0x0b, 0x0b, 
	                                0x0c, 0x0c, 0x0d, 0x0d, 0x0e, 0x0e, 0x0f, 0x0f };
	uint8_t decrypted_text[BLOCK_SIZE] = { 0 };

	LSX_Cipher obj;
	obj.SetRoundKeys(BLOCK_SIZE, NUMBER_OF_ROUNDS, key);

	for (plain_text[0] = 0; plain_text[0] < 16; plain_text[0]++)
	{
		for (plain_text[1] = 0; plain_text[1] < 16; plain_text[1]++)
		{
			printf("%u%u \n\n", plain_text[0], plain_text[1]);

			for (plain_text[2] = 0; plain_text[2] < 16; plain_text[2]++)
			{
				for (plain_text[3] = 0; plain_text[3] < 16; plain_text[3]++)
				{
					for (plain_text[4] = 0; plain_text[4] < 16; plain_text[4]++)
					{
						for (plain_text[5] = 0; plain_text[5] < 16; plain_text[5]++)
						{
							for (plain_text[6] = 0; plain_text[6] < 16; plain_text[6]++)
							{
								for (plain_text[7] = 0; plain_text[7] < 16; plain_text[7]++)
								{
									obj.EncryptBlock(plain_text, cipher_text);
									obj.DecryptBlock(cipher_text, decrypted_text);

									if (memcmp(plain_text, decrypted_text, BLOCK_SIZE))
									{
										printf("ERROR!!!");
										system("pause");
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

void LSX_Cipher_Test_Zero_Xor_Blocks()
{
	constexpr size_t NUMBER_OF_ROUNDS = 3;

	uint8_t plain_text[BLOCK_SIZE] = { 0 };
	uint8_t cipher_text[BLOCK_SIZE] = { 0 };
	uint8_t key[BLOCK_SIZE * 2] = { 0x08, 0x08, 0x09, 0x09, 0x0a, 0x0a, 0x0b, 0x0b, 
	                                0x0c, 0x0c, 0x0d, 0x0d, 0x0e, 0x0e, 0x0f, 0x0f };
	uint8_t decrypted_text[BLOCK_SIZE] = { 0 };

	uint8_t result[BLOCK_SIZE] = { 0 };

	LSX_Cipher obj;
	obj.SetRoundKeys(BLOCK_SIZE, NUMBER_OF_ROUNDS, key);

	for (plain_text[0] = 0; plain_text[0] < 4; plain_text[0]++)
	{
		for (plain_text[1] = 0; plain_text[1] < 2; plain_text[1]++)
		{
			printf("%x%x \n\n", plain_text[0], plain_text[1]);

			for (plain_text[2] = 0; plain_text[2] < 16; plain_text[2]++)
			{
				for (plain_text[3] = 0; plain_text[3] < 16; plain_text[3]++)
				{
					for (plain_text[4] = 0; plain_text[4] < 16; plain_text[4]++)
					{
						for (plain_text[5] = 0; plain_text[5] < 16; plain_text[5]++)
						{
							for (plain_text[6] = 0; plain_text[6] < 16; plain_text[6]++)
							{
								for (plain_text[7] = 0; plain_text[7] < 16; plain_text[7]++)
								{
									obj.EncryptBlock(plain_text, cipher_text);

									for (size_t i = 0; i < BLOCK_SIZE; i++)
									{
										result[i] ^= cipher_text[i];
									}
								}
							}
						}
					}
				}
			}
		}
	}

	printf("result: ");
	Print_Array(result);
}

void Test_Zero_Xor_Cipher_Texts_With_Skip_First_Round_After_Five_Rounds()
{
	constexpr size_t NUMBER_OF_ROUNDS = 5;

	uint8_t plain_text[BLOCK_SIZE] = { 0 };
	uint8_t cipher_text[BLOCK_SIZE] = { 0 };
	uint8_t key[BLOCK_SIZE * 2] = { 0x08, 0x08, 0x09, 0x09, 0x0a, 0x0a, 0x0b, 0x0b, 
	                                0x0c, 0x0c, 0x0d, 0x0d, 0x0e, 0x0e, 0x0f, 0x0f };

	LSX_Cipher obj;
	obj.SetRoundKeys(BLOCK_SIZE, NUMBER_OF_ROUNDS, key);

	uint8_t result[BLOCK_SIZE] = { 0 };

	//#pragma omp parallel for
	for (/*uint8_t*/__int8 i = 0; i < 4; i++)
	{
		plain_text[0] = i;
		plain_text[0] = InvSbox[plain_text[0]];
		plain_text[0] ^= 0x08;

		for (plain_text[1] = 0; plain_text[1] < 16; plain_text[1]++)
		{
			printf("%x%x \n\n", i, plain_text[1]);
			Print_Array(result);

			for (plain_text[2] = 0; plain_text[2] < 16; plain_text[2]++)
			{
				for (plain_text[3] = 0; plain_text[3] < 16; plain_text[3]++)
				{
					for (plain_text[4] = 0; plain_text[4] < 16; plain_text[4]++)
					{
						for (plain_text[5] = 0; plain_text[5] < 16; plain_text[5]++)
						{
							for (plain_text[6] = 0; plain_text[6] < 16; plain_text[6]++)
							{
								for (plain_text[7] = 0; plain_text[7] < 16; plain_text[7]++)
								{
									obj.EncryptBlock(plain_text, cipher_text);

									for (size_t i = 0; i < BLOCK_SIZE; i++)
									{
										result[i] ^= cipher_text[i];
									}
								}
							}
						}
					}
				}
			}
		}
	}

	printf("result: ");
	Print_Array(result);
}

void partial_sum_iteration(const std::vector<std::vector<bool>> &T_in, 
                           std::vector<std::vector<bool>> &T_out, 
                           size_t iteration_id, 
                           const uint8_t k)
{
	size_t shift = 32 - (12 + (4 * iteration_id));
	size_t shift2 = 32 - (8 + (4 * iteration_id));

	uint64_t mask_key = 0x000FFFFF << (28 - (4 * iteration_id)); //зависит от iteration_id
	uint64_t mask_ciphertext = 0x00000FFF >> (4 + (4 * iteration_id));

	for (uint8_t key_val = 0; key_val < 16; key_val++)
	{
		for (uint8_t row = 0; row < 16; row++) // Проходим по всем строкам матрицы
		{
			for (uint64_t j = 0; j < T_in[row].size(); j++) // Проходим по всем элементам в конкретной строке
			{
				if (T_in[row][j] == 1)
				{
					uint8_t cipher_text_val = (j >> shift) & 0x0f;
					uint8_t x_prev = (j >> shift2) & 0x0f;
					uint8_t x_new = x_prev ^ Mul_Matrix[InvL[iteration_id + 2][k]][InvSbox[cipher_text_val ^ key_val]];

					uint64_t i = (j & mask_key) | 
					             (uint64_t(key_val) << (shift + 4)) | 
					             (uint64_t(x_new) << (shift2 - 4)) | 
					             (j & mask_ciphertext);
					
					T_out[row][i] = T_out[row][i] ^ 1;
				}
			}
		}
	}
}

void Seven_Rounds_Attack()
{
	constexpr size_t NUMBER_OF_ROUNDS = 7;

	uint8_t plain_text[BLOCK_SIZE] = { 0 };
	uint8_t cipher_text[BLOCK_SIZE] = { 0 };
	uint8_t key[BLOCK_SIZE * 2] = { 0x08, 0x08, 0x09, 0x09, 0x0a, 0x0a, 0x0b, 0x0b, 
	                                0x0c, 0x0c, 0x0d, 0x0d, 0x0e, 0x0e, 0x0f, 0x0f };

	LSX_Cipher obj;
	obj.SetRoundKeys(BLOCK_SIZE, NUMBER_OF_ROUNDS, key);

	obj.Print_Round_Keys();

	std::vector<uint32_t> cipher_texts;

	for (uint8_t i = 0; i < 4; i++) // Переберём 2^30 шифр текстов
	{
		plain_text[0] = i;
		plain_text[0] = InvSbox[plain_text[0]]; // Пропускаем первый раунд
		plain_text[0] ^= 0x08;

		for (plain_text[1] = 0; plain_text[1] < 16; plain_text[1]++)
		{
			printf("%x%x \n\n", i, plain_text[1]);

			for (plain_text[2] = 0; plain_text[2] < 16; plain_text[2]++)
			{
				for (plain_text[3] = 0; plain_text[3] < 16; plain_text[3]++)
				{
					for (plain_text[4] = 0; plain_text[4] < 16; plain_text[4]++)
					{
						for (plain_text[5] = 0; plain_text[5] < 16; plain_text[5]++)
						{
							for (plain_text[6] = 0; plain_text[6] < 16; plain_text[6]++)
							{
								for (plain_text[7] = 0; plain_text[7] < 16; plain_text[7]++)
								{
									obj.Encrypt_Block_Equivalent_Presentation_Two_Last_Rounds(plain_text, cipher_text);

									uint32_t cipher = (uint32_t(cipher_text[0]) << 28) | 
									                  (uint32_t(cipher_text[1]) << 24) | 
									                  (uint32_t(cipher_text[2]) << 20) | 
									                  (uint32_t(cipher_text[3]) << 16) |
										              (uint32_t(cipher_text[4]) << 12) | 
										              (uint32_t(cipher_text[5]) << 8) | 
										              (uint32_t(cipher_text[6]) << 4) | 
										              (uint32_t(cipher_text[7]));

									cipher_texts.push_back(cipher);
								}
							}
						}
					}
				}
			}
		}
	}

	uint64_t size = std::pow(2, 32); // Размер двух bool таблиц T1 и T2

	std::vector<std::vector<bool>> T1(16, std::vector<bool>(size));
	std::vector<std::vector<bool>> T2(16, std::vector<bool>(size));

	for (size_t k = 0; k < 8; k++) // Проделываем операцию для всех полубайт ключа K'7
	{
		uint8_t Inv_K7[8] = { 0 };
		uint8_t Inv_K8[8] = { 0 };

		for (Inv_K8[0] = 0; Inv_K8[0] < 16; Inv_K8[0]++)
		{
			printf("Inv_K8[0]: %x \n", Inv_K8[0]);
			for (Inv_K8[1] = 0; Inv_K8[1] < 16; Inv_K8[1]++)
			{
				//#pragma omp parallel for
				for (/*size_t*/int64_t j = 0; j < cipher_texts.size(); j++)
				{
					uint8_t c0, c1;
					c0 = (cipher_texts[j] >> 28) & 0x0f;
					c1 = (cipher_texts[j] >> 24) & 0x0f;

					uint8_t x1 = Mul_Matrix[InvL[0][k]][InvSbox[c0 ^ Inv_K8[0]]] ^ 
					             Mul_Matrix[InvL[1][k]][InvSbox[c1 ^ Inv_K8[1]]];

					uint64_t i = (uint64_t(Inv_K8[1]) << 28) | 
					             (uint64_t(x1) << 24) | 
					             (uint64_t(cipher_texts[j]) & 0x0000000000ffffff);

					T1[Inv_K8[0]][i] = T1[Inv_K8[0]][i] ^ 1;
				}
			}
		}

		for (uint8_t count = 0; count < 6; count++)
		{
			partial_sum_iteration(T1, T2, count, k);
			T1 = T2;
			T2.clear();
			T2.resize(16);
			for (uint8_t i = 0; i < T2.size(); i++)
			{
				T2[i].resize(size);
			}
		}
	}
}

int main(int argc, char *argv[])
{
	LSX_Cipher_Test_1();
	LSX_Cipher_Test_All_Texts();
	LSX_Cipher_Test_Zero_Xor_Blocks();

	Five_Rounds_Atack_With_Equiv_Rep_Last_Round();
	Test_Zero_Xor_Cipher_Texts_Skip_First_After_Five_Round();
	Five_Round_Attack_Three_Distinguisher_Skip_First();
	Six_Rounds_Attack_With_Skipping_First_Round_And_Equiv_Rep_Last_Round();
	Test_Zero_Xor_Cipher_Texts_With_Skip_First_Round_After_Five_Rounds();
	Seven_Rounds_Attack();
	
	return 0;
}
